import './App.css';
import { Button1, Button2 } from './components/button';

function App() {
  return (
    <div style={{ margin: '50px' }} >
      <div className='row'>
        <div className='col-sm-6'>
          <div className='box'>
            <div className='display'>
              <input type="text" readonly size="18" />
            </div>

            <div className='keys'>
              <div className='button-row'>
                <Button2>DEL</Button2>
                <Button1>&#8592;</Button1>
                <Button1 bgColor={"#a7ded9f0"}>/</Button1>
              </div>
              <div className='button-row'>
                <Button1 color={"#1c9388f0"}>7</Button1>
                <Button1 color={"#1c9388f0"}>8</Button1>
                <Button1 color={"#1c9388f0"}>9</Button1>
                <Button1 bgColor={"#a7ded9f0"}>*</Button1>
              </div>
              <div className='button-row'>
                <Button1 color={"#1c9388f0"}>4</Button1>
                <Button1 color={"#1c9388f0"}>5</Button1>
                <Button1 color={"#1c9388f0"}>6</Button1>
                <Button1 bgColor={"#a7ded9f0"}>-</Button1>
              </div>
              <div className='button-row'>
                <Button1 color={"#1c9388f0"}>1</Button1>
                <Button1 color={"#1c9388f0"}>2</Button1>
                <Button1 color={"#1c9388f0"}>3</Button1>
                <Button1 bgColor={"#a7ded9f0"}>+</Button1>
              </div>
              <div className='button-row'>
                <Button1 color={"#1c9388f0"}>0</Button1>
                <Button1 color={"#1c9388f0"}>,</Button1>
               <Button2 bgColor={"#76c8c0f0"} color={'#fff'}>=</Button2>
              </div>

            </div>
          </div>
        </div>
        <div>
          
        </div>
      </div>

    </div>
  );
}

export default App;
