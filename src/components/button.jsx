import styled from "styled-components";

const Button1 = styled.button`
  width: 25%;  
  background: ${props => props.bgColor || "#dededef0"};  
  color: ${props => props.color || "#3A4655"};  
  padding: 20px;    
  font-size: 20px;  
  margin-right: 0px;  
  outline: none;
  border: 1px solid #fff;
  border-radius: 3px
`
const Button2 = styled.button`
  width: 50%;  
  background: ${props => props.bgColor || "#dededef0"};  
  color: ${props => props.color || "#3A4655"};
  padding: 20px;    
  font-size: 20px;  
  outline: none;
  border: none; 
  border: 1px solid #fff;
  border-radius: 3px
`

export { Button1,Button2 }